//
//  ICHAppDelegate.h
//  iChummer
//
//  Created by Marco Feltmann on 21.03.14.
//  Copyright (c) 2014 mfs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
