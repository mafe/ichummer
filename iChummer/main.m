//
//  main.m
//  iChummer
//
//  Created by Marco Feltmann on 21.03.14.
//  Copyright (c) 2014 mfs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ICHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ICHAppDelegate class]));
    }
}
